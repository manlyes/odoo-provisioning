# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v0.4.1] - 2020-04-24
### Added
- New role to add network settings.
  See [!88](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/88)

### Changed
- Add bash function that has been deleted from main script. Due to this backups were not working.
  See [!90](https://gitlab.com/coopdevs/odoo-provisioning/-/merge_requests/90)

## [v0.4.0] - 2020-01-28
### Added
- Create CHANGELOG.md with history from Gitlab releases.
  See [!85](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/85)
- Add [monitoring role](https://github.com/coopdevs/monitoring-role/tree/v0.3.0) for production environments. Defaults to yes.
  Please add the secrets described at [secrets-example.yml](https://github.com/coopdevs/monitoring-role/tree/add/promtail-odoo-template/defaults)
- Add pyenv config. If you already have your environment working, You  may want
  to set pyenv up to sync with upstream requirements and possible changes.
  See [!83](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/83)

### Changed
- Upgrade certbot_nginx to [v0.1.0](https://github.com/coopdevs/certbot_nginx/releases/tag/v0.1.0).
- Use certbot_nginx new flag to create new certs when in multidomain
- Upgrade odoo-role to [v0.1.8](https://github.com/coopdevs/odoo-role/releases/tag/v0.1.8).
  No more deprecation warnings in up-to-date inventories.
- Upgrade backups-role to [v1.2.6](https://github.com/coopdevs/backups_role/releases/tag/v1.2.6)
  Logging compatible with monitoring-role

### Removed
- Bind interface and proxy mode variables are now set automatically. Remove them
  from your inventory, please.
  See [!84](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/84)

## [v0.3.2] - 2019-09-16
### Changed
- [!82](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/82) Upgrade odoo role. Fixes upgrading comm. modules
- [!81](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/81) Fix typo in README.md

## [v0.3.1] - 2019-09-12
### Changed
- [!80](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/80) Enrich deprecation messages and add odoo_role_odoo_db_name
- [!79](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/79) Hotfix dbfilter: we roll back to db filtering only by subdomains %d, which has its limitations but works.
- [!78](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/78) Improve README

## [v0.3.0] - 2019-08-30
### Changed
- fea677cb Upgrade odoo-role to v0.1.5
- c4e104a6 and 1425c536 Update README.md

### Removed
- [!77](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/77)  Deprecate confusing vars

## [v0.2.3] - 2019-08-19
### Changed
- 4ca243c7 Hotfix for left-over deprecated var. Untested from 2c2d06a7.
- 2c2d06a7 Hotfix for some untested cases from [!76](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/76)
- [!76](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/76) Upgrade odoo-role from v0.1.2 to v0.1.3
- [!74](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/74) Import backups role instead of including. Otherwise --tags=install doesn't work
- [!69](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/69) Review odoo role and postgres vars (merged via cli)

## [v0.2.2] - 2019-07-16
### Changed
- [!73](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/73) Upgrade odoo-role version for multi-db full support
- [!72](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/72) Use domain_name and certbot_nginx_cert_name to generate multidomain certificate
- [!71](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/71) Upgrade odoo-role version. Multi DB related
- [!70](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/70) Use domains list instead a domain string in cerbot_nginx vars
- [!67](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/67) Adapt internal postgres role to multiple dbs

## [v0.2.1] - 2019-06-20
### Changed
- [!65](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/65)

## [v0.2.0] - 2019-05-14
### Changed
Among others, check that inventories are up-to-date with the playbooks version.

- [!59](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/59)
- [!61](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/61)
- [!62](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/62)
- [!63](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/63)
- [!64](https://gitlab.com/coopdevs/odoo-provisioning/merge_requests/64)

## [v0.1.0] - 2019-04-10
### Added
- First working version using backups_role from Ansible Galaxy

## [v0.0.2] - 2019-04-03
### Changed
- Fix critical requirements typo.

## [v0.0.1] - 2019-03-28
### Added
- First release! :rocket:

